import { NgModule } from '@angular/core';

import {
  MatToolbarModule, MatSidenavModule, MatTabsModule, MatBadgeModule,
  MatDialogModule, MatDatepickerModule, MatGridListModule, MatSelectModule,
  MatDividerModule, MatProgressBarModule, MatTooltipModule, MatMenuModule, MatChipsModule,
  MatPaginatorModule, MatCheckboxModule, MatProgressSpinnerModule, MatTableModule, MatExpansionModule, 
  MatListModule
} from '@angular/material';
import { MatButtonModule, MatInputModule, MatSnackBarModule, MatSliderModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatStepperModule } from '@angular/material/stepper';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatRadioModule} from '@angular/material/radio';

@NgModule({
  imports: [
    MatToolbarModule,
    MatFormFieldModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatSnackBarModule,
    MatIconModule,
    MatSidenavModule,
    MatTabsModule,
    MatBadgeModule,
    MatDialogModule,
    MatDatepickerModule,
    MatGridListModule,
    MatSelectModule,
    MatDividerModule,
    MatProgressBarModule,
    MatTooltipModule,
    MatMenuModule,
    MatChipsModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatSliderModule,
    MatStepperModule,
    MatExpansionModule,
    MatListModule,
    MatBottomSheetModule,
    MatSlideToggleModule,
    DragDropModule,
    MatRadioModule
  ],
  exports: [
    MatToolbarModule,
    MatFormFieldModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatSnackBarModule,
    MatIconModule,
    MatSidenavModule,
    MatTabsModule,
    MatBadgeModule,
    MatDialogModule,
    MatDatepickerModule,
    MatGridListModule,
    MatSelectModule,
    MatDividerModule,
    MatProgressBarModule,
    MatTooltipModule,
    MatMenuModule,
    MatChipsModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatSliderModule,
    MatStepperModule,
    MatTableModule,
    MatExpansionModule,
    MatListModule,
    MatBottomSheetModule,
    MatSlideToggleModule,
    DragDropModule,
    MatRadioModule
  ]
})
export class AppMaterialModule {

}
