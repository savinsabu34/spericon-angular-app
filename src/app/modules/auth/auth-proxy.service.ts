import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthProxyService {
  
  url = environment.API_URL;

  httpOptions = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin': '"*"',
      'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
      'Access-Control-Allow-Headers': 'X-Requested-With,content-type',
      'Access-Control-Allow-Credentials': 'true'
   })
};

  constructor(private http: HttpClient) { }
  
  login(loginForm): Observable<any> {
    return this.http.post(this.url + 'api/auth/signin', loginForm, this.httpOptions);
  }
}
