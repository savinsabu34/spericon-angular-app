import { Component, OnInit } from '@angular/core';

export interface UserTable {
  firstName: string;
  lastName: string;
  email: string;
}

const ELEMENT_DATA: UserTable[] = [
  {firstName: 'Savin', lastName: 'Sabu', email: 'some@gmail.com'},
  {firstName: 'Savin', lastName: 'Sabu', email: 'some@gmail.com'},
  {firstName: 'Savin', lastName: 'Sabu', email: 'some@gmail.com'},
  {firstName: 'Savin', lastName: 'Sabu', email: 'some@gmail.com'},
  {firstName: 'Savin', lastName: 'Sabu', email: 'some@gmail.com'},
  {firstName: 'Savin', lastName: 'Sabu', email: 'some@gmail.com'},
  {firstName: 'Savin', lastName: 'Sabu', email: 'some@gmail.com'},

  {firstName: 'Savin', lastName: 'Sabu', email: 'some@gmail.com'},
  {firstName: 'Savin', lastName: 'Sabu', email: 'some@gmail.com'},
  {firstName: 'Savin', lastName: 'Sabu', email: 'some@gmail.com'},
  {firstName: 'Savin', lastName: 'Sabu', email: 'some@gmail.com'},

  {firstName: 'Savin', lastName: 'Sabu', email: 'some@gmail.com'},

];


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  displayedColumns: string[] = ['firstName', 'lastName', 'email'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit() {
  }

}
