import { Component, OnInit } from '@angular/core';
import { LoginForm } from '../../entity/LoginForm';
import { AuthProxyService } from '../../auth-proxy.service';
import { isRegExp } from 'util';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private loginForm = new LoginForm();
  constructor(
    private authProxyService: AuthProxyService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  login(loginForm) {
    this.router.navigate(['login/dashboard']);
    this.authProxyService.login(loginForm).subscribe(res => {
      if(res) {
        this.router.navigate(['login/dashboard']);
      }
    })
  }
}
