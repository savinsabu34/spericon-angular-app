import { TestBed } from '@angular/core/testing';

import { UserProxyService } from './user-proxy.service';

describe('UserProxyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserProxyService = TestBed.get(UserProxyService);
    expect(service).toBeTruthy();
  });
});
